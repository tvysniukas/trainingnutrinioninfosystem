﻿using Autofac;
using Caliburn.Micro;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Windows;
using TrainingNutritionInfoSystem.Infrastructure;
using TrainingNutritionInfoSystem.Services;
using TrainingNutritionInfoSystem.ViewModels;

namespace TrainingNutritionInfoSystem;

public class Bootstrapper : BootstrapperBase
{
    private readonly string AppSettingsFileName = "appsettings.json";

    protected IContainer Container { get; private set; }

    public IConfiguration Configuration { get; private set; }

    public Bootstrapper()
    {
        InitializeConfiguration();
        Initialize();
    }

    protected override void Configure()
    {
        var builder = new ContainerBuilder();

        builder.RegisterInstance(Configuration).As<IConfiguration>();
        builder.Register(context =>
        {
            var optionsBuilder = new DbContextOptionsBuilder<AppDatabaseContext>();
            optionsBuilder.UseSqlite(Configuration.GetConnectionString("DefaultConnection"));
            return new AppDatabaseContext(optionsBuilder.Options);
        }).AsSelf().InstancePerLifetimeScope();

        builder.RegisterType<LoginManager>().As<ILoginManager>().SingleInstance();

        builder.RegisterType<AuthenticationService>()
               .AsSelf()
               .InstancePerLifetimeScope();

        builder.RegisterType<EventAggregator>().As<IEventAggregator>().SingleInstance();

        builder.RegisterType<WeightEntryManager>().SingleInstance();
        builder.RegisterType<MealScheduleManager>().As<IMealScheduleManager>().SingleInstance();
        builder.RegisterType<ExercisesService>().SingleInstance();

        builder.RegisterType<HomeViewModel>().SingleInstance();
        builder.RegisterType<WeightTrackerViewModel>().SingleInstance();
        builder.RegisterType<MealScheduleViewModel>().SingleInstance();
        builder.RegisterType<ExercisesViewModel>().SingleInstance();

        builder.RegisterType<LoginViewModel>().SingleInstance();
        builder.RegisterType<ShellViewModel>().SingleInstance();

        builder.RegisterType<WindowManager>().As<IWindowManager>().SingleInstance();

        Container = builder.Build();
    }

    protected override async void OnStartup(object sender, StartupEventArgs e)
    {
        await DisplayRootViewForAsync<LoginViewModel>();
        base.OnStartup(sender, e);
    }

    protected override object GetInstance(Type service, string key)
        => key is null ? Container.Resolve(service) : Container.ResolveNamed(key, service);

    protected sealed override IEnumerable<object> GetAllInstances(Type service)
        => Container.Resolve(typeof(IEnumerable<>).MakeGenericType(service)) as IEnumerable<object>;

    protected sealed override void BuildUp(object instance) => Container.InjectProperties(instance);

    protected override void OnExit(object sender, EventArgs e) => Container.Dispose();

    protected T Resolve<T>() => Container.Resolve<T>();

    private void InitializeConfiguration()
    {
        var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile(AppSettingsFileName, optional: false, reloadOnChange: true);

        Configuration = builder.Build();
    }
}
