﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace TrainingNutritionInfoSystem.Migrations
{
    /// <inheritdoc />
    public partial class Exercises : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Exercises",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Name = table.Column<string>(type: "TEXT", nullable: false),
                    Description = table.Column<string>(type: "TEXT", nullable: false),
                    Reps = table.Column<int>(type: "INTEGER", nullable: false),
                    Sets = table.Column<int>(type: "INTEGER", nullable: false),
                    RestInSeconds = table.Column<int>(type: "INTEGER", nullable: false),
                    ImagePath = table.Column<string>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Exercises", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "Exercises",
                columns: new[] { "Id", "Description", "ImagePath", "Name", "Reps", "RestInSeconds", "Sets" },
                values: new object[,]
                {
                    { 1, "Place your foot on the platform, aligned with your hip.\r\nUnlock the safety mechanism and press the platform all the way up until your leg is almost fully extended, without locking the knee. This is the starting position.\r\nLower the platform toward you, under control. Push back up to the starting position.\r\nYour heel should never come off the platform. Make sure to never lock your knee.\r\nEnsure to lock the safety pins properly once you are done, before lowering the platform.", "https://static.strengthlevel.com/images/exercises/single-leg-press/single-leg-press-800.jpg", "Single leg press", 10, 60, 3 },
                    { 2, "Grab a kettlebell with the opposite arm.\r\nOn one leg, bend forward at the hip by pushing the hips back. Keep the spine and neck neutral and hips leveled and think about driving the free leg's heel toward the ceiling.\r\nAlways keep a slightly flexed knee in the supporting leg.\r\nThe depth of the movement will depend of individual flexibility.\r\nTo go back up, squeeze the glutes hard and pull yourself with the posterior leg.\r\nKeep the shoulder tight and do not rotate the upperbody.", "https://barbend.com/wp-content/uploads/2017/04/1166685-e1551123037289.jpg", "KB 1 arm 1 leg RDL", 8, 60, 3 },
                    { 3, "Sit down on the seat and adjust the machine so you have about four fingers width from the end of the seat and the back of your knees.\r\nExtend the knees in control.", "https://www.burnthefatinnercircle.com/members/images/1636.jpg?cb=20240423044647", "Leg Extension", 8, 90, 4 },
                    { 4, "Lay on your stomach with your hip crease on the wedge.\r\nPut the pad behind your lower leg close to your ankles.\r\nKeep the head down and flex the knees to bring the heels as close as possible from your buttocks.\r\nDo not lift your hips off the bench as you pull.", "https://i.redd.it/t5o7gnn5p8la1.jpg", "Prone leg curl", 10, 50, 3 },
                    { 5, "Lie face down on a hyperextension bench with your hips positioned at the edge and your feet secured under the footpads. Cross your arms over your chest or place them behind your head.\r\nEngage your core and slowly raise your upper body, lifting your chest off the bench until your body forms a straight line from head to heels. Squeeze your lower back muscles at the top of the movement.\r\nLower your upper body back down to the starting position with control. Repeat for the desired number of repetitions, focusing on maintaining proper form and engaging your lower back muscles.", "https://www.lyfta.app/_next/image?url=https%3A%2F%2Flyfta.app%2Fimages%2Fexercises%2F04891101.png&w=640&q=10", "Hyperextensions", 15, 90, 2 },
                    { 6, "Sit down on the seat and adjust the machine so you have about four fingers width from the end of the seat and the back of your knees.\r\nFlex the knees in control.", "https://hips.hearstapps.com/hmg-prod/images/squat-jump-1585155002.jpg?crop=1.00xw:0.837xh;0,0.0987xh&resize=980:*", "Squat Jumps", 10, 60, 3 },
                    { 7, "Stand in one place and do small hops with different heights (a few hops at 30%, hops at 50%, and hops at 70% of their maximum).", "https://s3assets.skimble.com/assets/2151261/skimble-workout-trainer-exercise-pogo-hops-2_iphone.jpg", "Hops", 20, 90, 3 },
                    { 8, "Lie flat on a bench with your feet planted firmly on the ground. Hold a dumbbell in each hand, with your palms facing forward and your arms extended above your chest.\r\nEngage your core and lower the dumbbells towards your chest, bending your elbows and keeping them at a 45-degree angle to your body. Push the dumbbells back up to the starting position, fully extending your arms.", "https://static.strengthlevel.com/images/exercises/dumbbell-bench-press/dumbbell-bench-press-800.jpg", "Dumbbell Bench Press", 10, 60, 3 }
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Exercises");
        }
    }
}
