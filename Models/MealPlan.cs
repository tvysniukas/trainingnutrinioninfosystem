﻿namespace TrainingNutritionInfoSystem.Models;

public class MealPlan
{
    public int Id { get; set; }
    public string UserId { get; set; }
    public ApplicationUser User { get; set; }
    public DateTime Day { get; set; }
    public List<PlannedMeal> Meals { get; set; }
}
