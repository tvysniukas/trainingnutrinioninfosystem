﻿namespace TrainingNutritionInfoSystem.Models;

public class Meal
{
    public int Id { get; set; }
    public string Name { get; set; }
    public int Calories { get; set; }
    public string PortionSize { get; set; }
    public double? Protein { get; set; }

    public override string ToString()
        => $"{Name}, {Calories}kcal/{PortionSize}."
            + (Protein != null ? $" Protein: {Protein}g." : string.Empty);
}
