﻿namespace TrainingNutritionInfoSystem.Models;

public class PlannedMeal
{
    public int MealPlanId { get; set; }
    public MealPlan MealPlan { get; set; }

    public int MealId { get; set; }
    public Meal Meal { get; set; }
}