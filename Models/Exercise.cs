﻿namespace TrainingNutritionInfoSystem.Models;

public class Exercise
{
    public int Id { get; set; }
    public string Name { get; set; }
    public string Description { get; set; }
    public int Reps { get; set; }
    public int Sets { get; set; }
    public int RestInSeconds { get; set; }
    public string ImagePath { get; set; }
}
