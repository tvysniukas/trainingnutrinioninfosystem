﻿using Microsoft.AspNetCore.Identity;

namespace TrainingNutritionInfoSystem.Models;

public class ApplicationUser : IdentityUser
{
    public int Age { get; set; }
    public double Height { get; set; }
    public double WeightGoal { get; set; }
}