﻿namespace TrainingNutritionInfoSystem.Models;

public class WeightEntry
{
    public int Id { get; set; }
    public string Username { get; set; }
    public DateTime Day { get; set; }
    public double Weight { get; set; }
}

