﻿namespace TrainingNutritionInfoSystem.Infrastructure;

public class NavigateFromHomeEvent(NavigationTarget navigationTarget)
{
    public NavigationTarget NavigationTarget { get; } = navigationTarget;
}

public enum NavigationTarget
{
    WeightTracker,
    MealSchedule,
    Exercises,
    Home
}