﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using TrainingNutritionInfoSystem.Models;

namespace TrainingNutritionInfoSystem.Infrastructure;

public class AppDatabaseContext(DbContextOptions<AppDatabaseContext> options)
    : IdentityDbContext<ApplicationUser>(options)
{
    public DbSet<WeightEntry> WeightEntries { get; set; }
    public DbSet<Meal> Meals { get; set; }
    public DbSet<MealPlan> MealPlans { get; set; }
    public DbSet<Exercise> Exercises { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);

        modelBuilder.Entity<Meal>().ToTable("Meals");
        modelBuilder.Entity<MealPlan>()
            .ToTable("MealPlans");

        modelBuilder.Entity<MealPlan>()
            .HasOne<ApplicationUser>(mp => mp.User)
            .WithMany()
            .HasForeignKey(mp => mp.UserId);

        modelBuilder.Entity<PlannedMeal>()
            .HasKey(pm => new { pm.MealPlanId, pm.MealId });

        modelBuilder.Entity<PlannedMeal>()
            .HasOne(pm => pm.MealPlan)
            .WithMany(mp => mp.Meals)
            .HasForeignKey(pm => pm.MealPlanId);

        modelBuilder.Entity<PlannedMeal>()
            .HasOne(pm => pm.Meal)
            .WithMany()
            .HasForeignKey(pm => pm.MealId);

        modelBuilder.Entity<Meal>().HasData(
            new Meal { Id = 1, Name = "Sushi", Calories = 750, PortionSize = "16 pcs" },
            new Meal { Id = 2, Name = "Salad from Freshpost with Bacon", Calories = 640, PortionSize = "1" },
            new Meal { Id = 3, Name = "Cheese Curd with Crembrule", Calories = 560, PortionSize = "400g", Protein = 44 }
        );

        modelBuilder.Entity<MealPlan>().HasData(
            new MealPlan { Id = 1, UserId = "923124f2-69e6-49bc-b1b0-16cde1da878b", Day = DateTime.Parse("2024-05-01") },
            new MealPlan { Id = 2, UserId = "923124f2-69e6-49bc-b1b0-16cde1da878b", Day = DateTime.Parse("2024-05-02") }
        );

        modelBuilder.Entity<PlannedMeal>().HasData(
            new { MealPlanId = 1, MealId = 1 },
            new { MealPlanId = 1, MealId = 2 },
            new { MealPlanId = 2, MealId = 2 },
            new { MealPlanId = 2, MealId = 3 }
        );

                    modelBuilder
                .Entity<Exercise>()
                .HasData(
                new Exercise
                {
                    Id = 1,
                    Name = "Single leg press",
                    Description = """
                    Place your foot on the platform, aligned with your hip.
                    Unlock the safety mechanism and press the platform all the way up until your leg is almost fully extended, without locking the knee. This is the starting position.
                    Lower the platform toward you, under control. Push back up to the starting position.
                    Your heel should never come off the platform. Make sure to never lock your knee.
                    Ensure to lock the safety pins properly once you are done, before lowering the platform.
                    """,
                    Reps = 10,
                    Sets = 3,
                    RestInSeconds = 60,
                    ImagePath = "https://static.strengthlevel.com/images/exercises/single-leg-press/single-leg-press-800.jpg"
                },
                new Exercise
                {
                    Id = 2,
                    Name = "KB 1 arm 1 leg RDL",
                    Description = """
                    Grab a kettlebell with the opposite arm.
                    On one leg, bend forward at the hip by pushing the hips back. Keep the spine and neck neutral and hips leveled and think about driving the free leg's heel toward the ceiling.
                    Always keep a slightly flexed knee in the supporting leg.
                    The depth of the movement will depend of individual flexibility.
                    To go back up, squeeze the glutes hard and pull yourself with the posterior leg.
                    Keep the shoulder tight and do not rotate the upperbody.
                    """,
                    Reps = 8,
                    Sets = 3,
                    RestInSeconds = 60,
                    ImagePath = "https://barbend.com/wp-content/uploads/2017/04/1166685-e1551123037289.jpg"
                },
                new Exercise
                {
                    Id = 3,
                    Name = "Leg Extension",
                    Description = """
                    Sit down on the seat and adjust the machine so you have about four fingers width from the end of the seat and the back of your knees.
                    Extend the knees in control.
                    """,
                    Reps = 8,
                    Sets = 4,
                    RestInSeconds = 90,
                    ImagePath = "https://www.burnthefatinnercircle.com/members/images/1636.jpg?cb=20240423044647"
                },
                new Exercise
                {
                    Id = 4,
                    Name = "Prone leg curl",
                    Description = """
                    Lay on your stomach with your hip crease on the wedge.
                    Put the pad behind your lower leg close to your ankles.
                    Keep the head down and flex the knees to bring the heels as close as possible from your buttocks.
                    Do not lift your hips off the bench as you pull.
                    """,
                    Reps = 10,
                    Sets = 3,
                    RestInSeconds = 50,
                    ImagePath = "https://i.redd.it/t5o7gnn5p8la1.jpg"
                },
                new Exercise
                {
                    Id = 5,
                    Name = "Hyperextensions",
                    Description = """
                    Lie face down on a hyperextension bench with your hips positioned at the edge and your feet secured under the footpads. Cross your arms over your chest or place them behind your head.
                    Engage your core and slowly raise your upper body, lifting your chest off the bench until your body forms a straight line from head to heels. Squeeze your lower back muscles at the top of the movement.
                    Lower your upper body back down to the starting position with control. Repeat for the desired number of repetitions, focusing on maintaining proper form and engaging your lower back muscles.
                    """,
                    Reps = 15,
                    Sets = 2,
                    RestInSeconds = 90,
                    ImagePath = "https://www.lyfta.app/_next/image?url=https%3A%2F%2Flyfta.app%2Fimages%2Fexercises%2F04891101.png&w=640&q=10"
                },
                new Exercise
                {
                    Id = 6,
                    Name = "Squat Jumps",
                    Description = """
                    Sit down on the seat and adjust the machine so you have about four fingers width from the end of the seat and the back of your knees.
                    Flex the knees in control.
                    """,
                    Reps = 10,
                    Sets = 3,
                    RestInSeconds = 60,
                    ImagePath = "https://hips.hearstapps.com/hmg-prod/images/squat-jump-1585155002.jpg?crop=1.00xw:0.837xh;0,0.0987xh&resize=980:*"
                },
                new Exercise
                {
                    Id = 7,
                    Name = "Hops",
                    Description = """
                    Stand in one place and do small hops with different heights (a few hops at 30%, hops at 50%, and hops at 70% of their maximum).
                    """,
                    Reps = 20,
                    Sets = 3,
                    RestInSeconds = 90,
                    ImagePath = "https://s3assets.skimble.com/assets/2151261/skimble-workout-trainer-exercise-pogo-hops-2_iphone.jpg"
                },
                new Exercise
                {
                    Id = 8,
                    Name = "Dumbbell Bench Press",
                    Description = """
                    Lie flat on a bench with your feet planted firmly on the ground. Hold a dumbbell in each hand, with your palms facing forward and your arms extended above your chest.
                    Engage your core and lower the dumbbells towards your chest, bending your elbows and keeping them at a 45-degree angle to your body. Push the dumbbells back up to the starting position, fully extending your arms.
                    """,
                    Reps = 10,
                    Sets = 3,
                    RestInSeconds = 60,
                    ImagePath = "https://static.strengthlevel.com/images/exercises/dumbbell-bench-press/dumbbell-bench-press-800.jpg"
                }
    );
    }
}
