﻿namespace TrainingNutritionInfoSystem.Infrastructure
{
    public enum WeightGoalOption
    {
        Lose,
        Maintain,
        Gain
    }
}
