﻿using Microsoft.EntityFrameworkCore;
using TrainingNutritionInfoSystem.Infrastructure;
using TrainingNutritionInfoSystem.Models;

namespace TrainingNutritionInfoSystem.Services;

public class MealScheduleManager(AppDatabaseContext context, AuthenticationService authenticationService) : IMealScheduleManager
{
    public async Task<IEnumerable<MealPlan>> GetAllMealPlans()
        => await context.MealPlans
                        .Include(mp => mp.Meals)
                        .ThenInclude(m => m.Meal)
                        .Where(mp => mp.UserId == authenticationService.CurrentUser.Id)
                        .ToListAsync();

    public async Task<MealPlan?> GetMealPlanById(int mealPlanId)
        => await context.MealPlans.Include(mp => mp.Meals).FirstOrDefaultAsync(mp => mp.Id == mealPlanId);

    public async Task AddMealPlan(MealPlan mealPlan)
    {
        context.MealPlans.Add(mealPlan);
        await context.SaveChangesAsync();
    }

    public async Task UpdateMealPlan(MealPlan mealPlan)
    {
        context.MealPlans.Update(mealPlan);
        await context.SaveChangesAsync();
    }

    public async Task DeleteMealPlan(int mealPlanId)
    {
        var mealPlan = await context.MealPlans.FindAsync(mealPlanId);
        if (mealPlan != null)
        {
            context.MealPlans.Remove(mealPlan);
            await context.SaveChangesAsync();
        }
    }

    public async Task<IEnumerable<Meal>> GetAllMeals()
        => await context.Meals.ToListAsync();

    public async Task<Meal?> GetMealById(int mealId)
        => await context.Meals.FindAsync(mealId);

    public async Task AddMeal(Meal meal)
    {
        context.Meals.Add(meal);
        await context.SaveChangesAsync();
    }

    public async Task UpdateMeal(Meal meal)
    {
        context.Meals.Update(meal);
        await context.SaveChangesAsync();
    }

    public async Task DeleteMeal(int mealId)
    {
        var meal = await context.Meals.FindAsync(mealId);
        if (meal != null)
        {
            context.Meals.Remove(meal);
            await context.SaveChangesAsync();
        }
    }
}

public interface IMealScheduleManager
{
    Task<IEnumerable<MealPlan>> GetAllMealPlans();
    Task<MealPlan?> GetMealPlanById(int mealPlanId);
    Task AddMealPlan(MealPlan mealPlan);
    Task UpdateMealPlan(MealPlan mealPlan);
    Task DeleteMealPlan(int mealPlanId);

    Task<IEnumerable<Meal>> GetAllMeals();
    Task<Meal?> GetMealById(int mealId);
    Task AddMeal(Meal meal);
    Task UpdateMeal(Meal meal);
    Task DeleteMeal(int mealId);
}

