﻿using Microsoft.EntityFrameworkCore;
using TrainingNutritionInfoSystem.Infrastructure;
using TrainingNutritionInfoSystem.Models;

namespace TrainingNutritionInfoSystem.Services;

public class ExercisesService(AppDatabaseContext appDatabaseContext)
{
    public List<Exercise> GetAllExercises()
        => appDatabaseContext.Exercises.ToList();

    public List<Exercise> GetRandomExercises(int amount)
        => appDatabaseContext.Exercises
                             .OrderBy(e => EF.Functions.Random())
                             .Take(amount)
                             .ToList();


    public void AddNewExercise(Exercise newExercise)
    {
        appDatabaseContext.Exercises.Add(newExercise);
        appDatabaseContext.SaveChanges();
    }
}
