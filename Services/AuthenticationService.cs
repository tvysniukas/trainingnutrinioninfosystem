﻿using Caliburn.Micro;
using TrainingNutritionInfoSystem.Infrastructure;
using TrainingNutritionInfoSystem.Models;

namespace TrainingNutritionInfoSystem.Services;

public class AuthenticationService(AppDatabaseContext appDatabaseContext)
{
    public ApplicationUser? CurrentUser { get; private set; }

    public async Task<bool> RegisterUser(
        string username,
        string password,
        int age,
        double height)
    {
        var user = new ApplicationUser
        {
            UserName = username,
            PasswordHash = password,
            Age = age,
            Height = height,
        };

        appDatabaseContext.Users.Add(user);
        await appDatabaseContext.SaveChangesAsync();
        CurrentUser = user;

        return true;
    }

    public bool LoginUser(string email, string password)
    {
        CurrentUser = appDatabaseContext.Users.FirstOrDefault(x => x.UserName == email && x.PasswordHash == password);
        return CurrentUser is not null;
    }
}
