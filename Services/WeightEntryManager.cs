﻿using TrainingNutritionInfoSystem.Infrastructure;
using TrainingNutritionInfoSystem.Models;

namespace TrainingNutritionInfoSystem.Services;

public class WeightEntryManager(AppDatabaseContext appDatabaseContext)
{
    public void AddWeightEntry(WeightEntry entry)
    {
        appDatabaseContext.WeightEntries.Add(entry);
        appDatabaseContext.SaveChanges();
    }

    public List<WeightEntry> GetAllEntries()
        => appDatabaseContext.WeightEntries.ToList();
}
