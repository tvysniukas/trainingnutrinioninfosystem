﻿using System.Globalization;
using System.Windows.Data;
using TrainingNutritionInfoSystem.Models;

namespace TrainingNutritionInfoSystem.Utilities;

public class TotalCaloriesConverter : IValueConverter
{
    private const string FallbackValue = "N/A";

    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        => value is MealPlan mealPlan ? mealPlan.Meals.Sum(m => m.Meal.Calories) : FallbackValue;

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        => throw new NotImplementedException();
}
