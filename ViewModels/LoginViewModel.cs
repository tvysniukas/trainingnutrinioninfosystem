﻿using Caliburn.Micro;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using TrainingNutritionInfoSystem.Infrastructure;
using TrainingNutritionInfoSystem.Services;

namespace TrainingNutritionInfoSystem.ViewModels;

public class LoginViewModel : Screen,
    INotifyPropertyChanged,
    IHandle<LogoutEvent>
{
    private readonly IWindowManager _windowManager;
    private readonly IEventAggregator _eventAggregator;
    private readonly ShellViewModel _shellViewModel;
    private readonly AuthenticationService _authenticationService;

    public LoginViewModel(IWindowManager windowManager,
        ShellViewModel shellViewModel,
        AuthenticationService authenticationService,
        IEventAggregator eventAggregator)
    {
        _windowManager = windowManager;
        _shellViewModel = shellViewModel;
        _authenticationService = authenticationService;

        _eventAggregator = eventAggregator;
        _eventAggregator.SubscribeOnPublishedThread(this);
    }

    public string Username { get; set; } = "tvysniukas@gmail.com";
    public string Password { private get; set; } = string.Empty;

    public int Age { get; set; } = 14;
    public double Height { get; set; } = 1.20;

    public bool IsRegistering { get; set; } = false;

    public Visibility RegisterInputFieldsVisibility
        => IsRegistering ? Visibility.Visible : Visibility.Collapsed;

    public string FormTitle
        => IsRegistering ? "Register" : "Log in";

    public string LogInButtonText
        => IsRegistering ? "Back to log in" : "Log in";

    public string RegisterButtonText
        => IsRegistering ? "Register & log in" : "New here? Register!";

    public async Task HandleAsync(LogoutEvent message, CancellationToken cancellationToken)
    {
        await _windowManager.ShowWindowAsync(this);
    }

    public async Task OnLoginButtonClicked()
    {
        if(IsRegistering)
        {
            IsRegistering = false;
            return;
        }

        Mouse.OverrideCursor = Cursors.Wait;
        try
        {
            if (!AreInputsValid())
            {
                return;
            }

            if (_authenticationService.LoginUser(email: Username, password: Password))
            {
                await _windowManager.ShowWindowAsync(_shellViewModel);
                await TryCloseAsync();
            }
            else
            {
                MessageBox.Show("Error", "Invalid credentials", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        finally
        {
            Password = string.Empty;
            Mouse.OverrideCursor = null;
        }
    }

    public async Task OnRegisterButtonClicked()
    {
        if(!IsRegistering)
        {
            IsRegistering = true;
            return;
        }
        Mouse.OverrideCursor = Cursors.Wait;

        try
        { 
            bool isRegistrationSuccessful = await _authenticationService.RegisterUser(username: Username,
                password: Password,
                age: Age,
                height: Height);

            if (isRegistrationSuccessful)
            {
                await _windowManager.ShowWindowAsync(_shellViewModel);
                await TryCloseAsync();
            }
        }
        finally
        {
            Password = string.Empty;
            IsRegistering = false;

            Mouse.OverrideCursor = null;
        }
    }

    private bool AreInputsValid()
        => !string.IsNullOrWhiteSpace(Username) && !string.IsNullOrWhiteSpace(Password);
}

