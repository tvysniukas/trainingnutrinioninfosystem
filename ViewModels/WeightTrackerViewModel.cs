﻿using Caliburn.Micro;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using System.ComponentModel;
using TrainingNutritionInfoSystem.Models;
using TrainingNutritionInfoSystem.Services;

namespace TrainingNutritionInfoSystem.ViewModels;

public class WeightTrackerViewModel : Screen, INotifyPropertyChanged
{
    private readonly WeightEntryManager _weightEntryManager;
    private readonly AuthenticationService _authenticationService;
    private IEnumerable<WeightEntry> _allWeightEntries;

    public PlotModel PlotModel { get; set; }

    public double Weight { get; set; }

    public DateTime EntryDate { get; set; } = DateTime.Today;

    public WeightTrackerViewModel(
        WeightEntryManager weightEntryManager,
        AuthenticationService authenticationService)
    {
        DisplayName = "Weight Tracker";

        _weightEntryManager = weightEntryManager;
        _authenticationService = authenticationService;
        CreatePlotModel();
    }

    protected override async Task OnActivateAsync(CancellationToken cancellationToken)
    {
        RefreshUI();
        await base.OnActivateAsync(cancellationToken);
    }

    public void LogWeight()
    {
        var entry = new WeightEntry
        {
            Username = _authenticationService.CurrentUser.UserName,
            Day = EntryDate,
            Weight = Weight
        };
        _weightEntryManager.AddWeightEntry(entry);
        RefreshUI();
    }

    private void RefreshUI()
    {
        _allWeightEntries = _weightEntryManager
            .GetAllEntries()
            .Where(entry => entry.Username == _authenticationService.CurrentUser.UserName)
            .OrderBy(entry => entry.Day);
        if (_allWeightEntries.Any())
        {
            Weight = _allWeightEntries.Last().Weight;
            UpdateModel();
            PlotModel.InvalidatePlot(true);
        }
    }

    private void UpdateModel()
    {
        LineSeries series = UpdateDataPoints();
        PlotModel!.Series.Clear();
        PlotModel!.Series.Add(series);
    }

    private LineSeries UpdateDataPoints()
    {
        var series = new LineSeries();

        foreach (var entry in _allWeightEntries)
        {
            series.Points.Add(new DataPoint(DateTimeAxis.ToDouble(entry.Day), entry.Weight));
        }

        return series;
    }

    private void CreatePlotModel()
    {
        PlotModel = new PlotModel();

        PlotModel.Axes.Add(new DateTimeAxis
        {
            Position = AxisPosition.Bottom,
            StringFormat = "yyyy-MM-dd",
            Title = "Date",
            MinorIntervalType = DateTimeIntervalType.Days,
            IntervalType = DateTimeIntervalType.Days,
            MajorGridlineStyle = LineStyle.Solid,
            MinorGridlineStyle = LineStyle.Dot
        });

        PlotModel.Axes.Add(new LinearAxis
        {
            Position = AxisPosition.Left,
            Title = "Weight (kg)"
        });
    }
}
