﻿using Caliburn.Micro;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using TrainingNutritionInfoSystem.Infrastructure;
using TrainingNutritionInfoSystem.Models;
using TrainingNutritionInfoSystem.Services;

namespace TrainingNutritionInfoSystem.ViewModels;

public class MealScheduleViewModel : Screen, INotifyPropertyChanged
{
    private readonly IMealScheduleManager _mealScheduleManager;
    private readonly AuthenticationService _authenticationService;
    private readonly WeightEntryManager _weightEntryManager;
    private const int _caloriesPerKilogram = 7700;
    private const double _noPhysicalActivityMultiplier = 1.2;

    public ObservableCollection<MealPlan> Schedule { get; set; } = [];
    public DateTime NewPlannedMealDate { get; set; } = DateTime.Today;
    public Meal NewPlannedMeal { get; set; } = new();
    public Meal NewMeal { get; set; } = new();
    public ObservableCollection<WeightGoalOption> WeightGoalOptions { get; set; } =
    [
        WeightGoalOption.Lose,
        WeightGoalOption.Maintain,
        WeightGoalOption.Gain
    ];
    public WeightGoalOption SelectedWeightGoal { get; set; } = WeightGoalOption.Maintain;
    public ObservableCollection<double> WeightChangeOptions { get; set; } = [0.25, 0.5, 1];
    public double? SelectedWeightChange { get; set; }
    public int RecommendedCalories { get; set; } = 0;
    public bool ShouldWeightChange => SelectedWeightGoal is not WeightGoalOption.Maintain;
    public Visibility RecommendationVisibility => RecommendedCalories > 0 ? Visibility.Visible : Visibility.Hidden;
    public ObservableCollection<Meal> AllMeals { get; set; } = [];

    public MealScheduleViewModel(IMealScheduleManager mealScheduleManager,
        AuthenticationService authenticationService,
        WeightEntryManager weightEntryManager)
    {
        DisplayName = "Meal Schedule";

        _mealScheduleManager = mealScheduleManager;
        _authenticationService = authenticationService;
        _weightEntryManager = weightEntryManager;
    }

    protected override async Task OnActivateAsync(CancellationToken cancellationToken)
    {
        await UpdateSchedule();
        AllMeals = new(await _mealScheduleManager.GetAllMeals());
        await base.OnActivateAsync(cancellationToken);
    }

    private async Task UpdateSchedule()
    {
        IEnumerable<MealPlan> allMealPlans = await _mealScheduleManager.GetAllMealPlans();
        allMealPlans = allMealPlans.OrderBy(mp => mp.Day);
        Schedule = new(allMealPlans);
    }

    public async Task OnPlanMealButtonClick()
    {
        MealPlan? currentMealPlan = Schedule.FirstOrDefault(mp => mp.Day == NewPlannedMealDate);

        if (currentMealPlan is not null)
        {
            currentMealPlan.Meals.Add(new PlannedMeal { Meal = NewPlannedMeal });

            await _mealScheduleManager.UpdateMealPlan(currentMealPlan);
        }
        else
        {
            var newMealPlan = new MealPlan
            {
                Day = NewPlannedMealDate,
                UserId = _authenticationService.CurrentUser.Id,
                Meals = [new PlannedMeal { Meal = NewPlannedMeal }]
            };

            await _mealScheduleManager.AddMealPlan(newMealPlan);
        }

        await UpdateSchedule();
    }

    public async Task OnAddMealButtonClick()
    {
        await _mealScheduleManager.AddMeal(NewMeal);
        AllMeals = new(await _mealScheduleManager.GetAllMeals());
    }

    public void OnCalculateButtonClick()
    {
        double weight = GetLatestWeight();
        double heightInCm = _authenticationService.CurrentUser.Height * 100;
        int age = _authenticationService.CurrentUser.Age;
        string gender = "Male";

        double basalMetabolicRate = CalculateBasalMetabolicRate(weight, heightInCm, age, gender);
        double dailyCaloriesRecommendation = CalculateDailyCaloriesRecommendation(basalMetabolicRate);

        RecommendedCalories = (int)Math.Round(dailyCaloriesRecommendation);
    }

    private double GetLatestWeight() =>
        _weightEntryManager.GetAllEntries()
            .OrderBy(entry => entry.Day)
            .LastOrDefault()?.Weight ?? throw new InvalidOperationException("No weight entries found.");

    private static double CalculateBasalMetabolicRate(double weight, double heightInCm, int age, string gender)
        => gender == "Male"
            ? 10 * weight + 6.25 * heightInCm - 5 * age + 5
            : 10 * weight + 6.25 * heightInCm - 5 * age - 161;

    private double CalculateDailyCaloriesRecommendation(double basalMetabolicRate)
    {
        double dailyCaloriesRecommendation = basalMetabolicRate * _noPhysicalActivityMultiplier;
        if (SelectedWeightChange.HasValue && ShouldWeightChange)
        {
            double weightChangeCalories = CalculateWeightChangeCalories();
            dailyCaloriesRecommendation += weightChangeCalories;
        }

        return dailyCaloriesRecommendation;
    }

    private double CalculateWeightChangeCalories() =>
        SelectedWeightGoal == WeightGoalOption.Lose
            ? -_caloriesPerKilogram * SelectedWeightChange!.Value / 7
            : _caloriesPerKilogram * SelectedWeightChange!.Value / 7;
}
