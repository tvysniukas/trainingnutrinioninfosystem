﻿using Caliburn.Micro;
using System.ComponentModel;
using TrainingNutritionInfoSystem.Infrastructure;

namespace TrainingNutritionInfoSystem.ViewModels;

public class HomeViewModel : Screen, INotifyPropertyChanged
{
    public readonly IEventAggregator _eventAggregator;
    private readonly IWindowManager _windowManager;

    public HomeViewModel(
        IEventAggregator eventAggregator,
        IWindowManager windowManager)
    {
        DisplayName = "Home";

        _eventAggregator = eventAggregator;
        _windowManager = windowManager;
    }

    public async Task NavigateToWeightTracking()
        => await _eventAggregator.PublishOnUIThreadAsync(
            new NavigateFromHomeEvent(NavigationTarget.WeightTracker));

    public async Task NavigateToMealPlanning()
        => await _eventAggregator.PublishOnUIThreadAsync(
            new NavigateFromHomeEvent(NavigationTarget.MealSchedule));

    public async Task NavigateToExercising()
        => await _eventAggregator.PublishOnUIThreadAsync(
            new NavigateFromHomeEvent(NavigationTarget.Exercises));

    public async Task Logout()
    {
        await _eventAggregator.PublishOnUIThreadAsync(new LogoutEvent());
        await TryCloseAsync();
    }
}
