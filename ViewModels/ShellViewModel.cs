﻿using Caliburn.Micro;
using TrainingNutritionInfoSystem.Infrastructure;

namespace TrainingNutritionInfoSystem.ViewModels;

public class ShellViewModel
    : Conductor<IScreen>.Collection.OneActive,
    IHandle<NavigateFromHomeEvent>,
    IHandle<LogoutEvent>
{
    private int _selectedIndex;
    private readonly WeightTrackerViewModel _weightTrackerViewModel;
    private readonly MealScheduleViewModel _mealScheduleViewModel;
    private readonly ExercisesViewModel _exercisesViewModel;
    private readonly HomeViewModel _homeViewModel;
    private readonly IEventAggregator _eventAggregator;

    public ShellViewModel(
        WeightTrackerViewModel weightTrackerViewModel,
        MealScheduleViewModel mealScheduleViewModel,
        ExercisesViewModel exercisesViewModel,
        HomeViewModel homeViewModel,
        IEventAggregator eventAggregator)
    {
        _weightTrackerViewModel = weightTrackerViewModel;
        _mealScheduleViewModel = mealScheduleViewModel;
        _exercisesViewModel = exercisesViewModel;
        _homeViewModel = homeViewModel;

        _eventAggregator = eventAggregator;
        _eventAggregator.SubscribeOnPublishedThread(this);
    }

    public int SelectedIndex
    {
        get => _selectedIndex;
        set => Set(ref _selectedIndex, value);
    }

    public static string WindowTitle => "Information System for Individual Training and Nutrition Planning";

    protected override void OnViewAttached(object view, object context)
    {
        Items.AddRange(
            [
                _homeViewModel,
                _weightTrackerViewModel,
                _mealScheduleViewModel,
                _exercisesViewModel,
            ]);
        base.OnViewAttached(view, context);
    }

    protected override async Task OnActivateAsync(CancellationToken cancellationToken)
    {
        SetDefaultTabSelectedIndex();
        await ActivateItemAsync(Items[SelectedIndex], cancellationToken);

        await base.OnActivateAsync(cancellationToken);
    }

    public void SetDefaultTabSelectedIndex()
        => SelectedIndex = Items.IndexOf(_homeViewModel);

    public async Task HandleAsync(NavigateFromHomeEvent message, CancellationToken cancellationToken)
    {
        Screen itemToActivate = message.NavigationTarget switch
        {
            NavigationTarget.WeightTracker => _weightTrackerViewModel,
            NavigationTarget.MealSchedule => _mealScheduleViewModel,
            NavigationTarget.Exercises => _exercisesViewModel,
            _ => _homeViewModel
        };

        SelectedIndex = Items.IndexOf(itemToActivate);
        await ActivateItemAsync(Items[SelectedIndex], cancellationToken);
    }

    public async Task HandleAsync(LogoutEvent message, CancellationToken cancellationToken)
        => await TryCloseAsync();
}

