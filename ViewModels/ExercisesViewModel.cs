﻿using Caliburn.Micro;
using System.Collections.ObjectModel;
using System.ComponentModel;
using TrainingNutritionInfoSystem.Models;
using TrainingNutritionInfoSystem.Services;

namespace TrainingNutritionInfoSystem.ViewModels;

public class ExercisesViewModel : Screen, INotifyPropertyChanged
{
    private readonly ExercisesService _exercisesService;

    public ObservableCollection<Exercise> Exercises { get; set; } = [];

    public Exercise NewExercise { get; set; } = InitializeNewExercise();

    public ExercisesViewModel(ExercisesService exercisesService)
    {
        DisplayName = "Exercises";

        _exercisesService = exercisesService;
    }

    protected override async Task OnInitializeAsync(CancellationToken cancellationToken)
    {
        GetNewListOfExercises();
        await base.OnActivateAsync(cancellationToken);
    }

    public void OnAddExcerciseClicked()
    {
        _exercisesService.AddNewExercise(NewExercise);
        NewExercise = InitializeNewExercise();
    }

    public static Exercise InitializeNewExercise()
        => new()
        {
            Reps = 0,
            Sets = 0,
            RestInSeconds = 0
        };

    public void OnRefreshListClicked() => GetNewListOfExercises();

    public void GetNewListOfExercises()
        => Exercises = new(_exercisesService.GetRandomExercises(amount: 4));
}
