﻿namespace TrainingNutritionInfoSystem;

public partial class App
{
    private static Bootstrapper? _bootstrapper;

    public App()
    {
        _bootstrapper = new Bootstrapper();
        _bootstrapper.Initialize();
    }
}
